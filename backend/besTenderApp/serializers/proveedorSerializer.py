from rest_framework import serializers
from besTenderApp.models.proveedor import Proveedor


class ProveedorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedor
        fields = ['idProveedor', 'nombreProveedor', 'nitProveedor', 'emailProveedor','celularProveedor','nombreContacto','nombreEmpresa']

    def to_representation(self, obj):
        proveedor = Proveedor.objects.get(id=obj.idProveedor)

        return {
            "idProveedor": proveedor.idProveedor,
            "nombreProveedor": proveedor.nombreProveedor,
            "nitProveedor": proveedor.nitProveedor,
            "emailProveedor": proveedor.emailProveedor,
            "celularProveedor": proveedor.celularProveedor,
            "nombreContacto": proveedor.nombreContacto,
            "nombreEmpresa": proveedor.nombreEmpresa
        }
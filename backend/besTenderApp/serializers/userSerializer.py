from besTenderApp.models.user import User
from besTenderApp.models.inventario import Inventario
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    inventario = None

    class Meta:
        model = User
        fields = ['idUser', 'username', 'password', 'nombre', 'email', 'celular', 'nombreTienda']

        def create(self, validated_date):
            userInstance = User.objects.create(**validated_date)
            return userInstance
        def to_representation(self, obj):
            user = User.objects.get(id=obj.idUser)
            return {
                "idUser": user.idUser,
                "username": user.username,
                "password": user.password,
                "nombre": user.nombre,
                "email": user.email,
                "celular": user.celular,
                "nombreTienda": user.nombreTienda,
            }

from django.db import models

from besTenderApp.models.inventario import Inventario
from besTenderApp.models.user import User


class Transaccion (models.Model):
    idTransaccion = models.AutoField(primary_key=True)
    idInventario = models.ForeignKey(Inventario, on_delete= models.CASCADE)
    idUser= models.ForeignKey(User, on_delete= models.CASCADE)
    cantidad = models.IntegerField(default=0)
    fechaTransaccion = models.DateField(
        'Fecha de transacción', blank=False, null=False)
    tipoTransaccion = models.CharField(max_length=10) # aquí se define si es compra o venta
    precioTotal = models.IntegerField(default=0)
from django.db import models
from besTenderApp.models.proveedor import Proveedor


class Producto (models.Model):
    idProducto = models.AutoField(primary_key=True)
    nombreProducto = models.CharField(max_length=30)
    idProveedor = models.ForeignKey(Proveedor, related_name='proveedor', on_delete=models.CASCADE)
    idTransaccion = models.ForeignKey(Proveedor, related_name='transaccion', on_delete=models.CASCADE)
    categoriaProducto = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300)
    precio = models.CharField(max_length=30)
    fechaVencimiento = models.DateField()
from django.db import models

class Proveedor (models.Model):
    idProveedor = models.AutoField(primary_key=True)
    nombreProveedor = models.CharField(max_length=30, unique=True)
    nitProveedor = models.CharField(max_length=10)
    emailProveedor = models.EmailField('Email', max_length=100, blank=True, null=True)
    celularProveedor = models.CharField(max_length=30, blank=True, null=True)
    nombreContacto = models.CharField(max_length=30, blank=True, null=True)
    nombreEmpresa = models.CharField(max_length=30, blank=True, null=True)